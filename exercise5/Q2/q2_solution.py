
def find_maximum(non_negative_numbers_array):
    if len(non_negative_numbers_array) == 0:
        return -1

    if len(non_negative_numbers_array) == 1:
        return non_negative_numbers_array[0]

    first_number = non_negative_numbers_array[0]
    rest_off_array = non_negative_numbers_array[1:]

    return max(first_number, find_maximum(rest_off_array))



# test code
print(find_maximum([3,4,1]))
print(find_maximum([9,3,0,10]))
print(find_maximum([9,3,0,]))
print(find_maximum([]))
print(find_maximum([5]))
print(find_maximum([10,9,8]))

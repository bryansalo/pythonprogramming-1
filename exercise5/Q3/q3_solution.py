def is_palindrome(input_string):
    # empty string is not a palindrome
    if len(input_string) == 0:
        return False

    if len(input_string) == 1:
        return True

    if len(input_string) == 2:
        return input_string[0] == input_string[1]

    return input_string[0] == input_string[-1] and is_palindrome(input_string[1:-1])



print("true:  ", is_palindrome("a"))
print("false: ", is_palindrome("ab"))
print("true:  ", is_palindrome("aba"))
print("false: ", is_palindrome("abcd"))
print("true:  ", is_palindrome("abba"))
print("true:  ", is_palindrome("abcba"))

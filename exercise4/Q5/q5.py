def simple_sent_analysis(in_file):

    # read in_file
    # go over each line
    # substitue !?, -;$%. with a single space
    # change to lowercase
    # split the line to words array
    # count happy/sad

    try:
        file = open(in_file, 'r')

    except OSError:
        print("Cannot encode",in_file,"due to IO error")
        return

    with file:

        happy_count = 0
        sad_count = 0

        for line in file:
            line = line.lower()
            line = substitue(line, "!?,-$%.", " ")
            list_of_words = line.split()            
            happy = list_of_words.count("happy")
            happy_count += happy
            sad = list_of_words.count("sad")
            sad_count += sad

        file.close()
        return {"happy": happy_count, "sad": sad_count}

def substitue(line, remove_string, replace_string):
    for c in remove_string:
        line = line.replace(c, " ")
    return line




#test
dic = simple_sent_analysis("test.txt")
print(dic)

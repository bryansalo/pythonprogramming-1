def find_substring_locations(s, k):

    # validating the input
    if k > len(s):
        return None
    if k < 1:
        return None

    substr_count_dictionary = {} # result dictionary
    for i in range(len(s)+1-k):
        # extract every substring of length k
        substr = s[i:i+k]

        # count each substring
        count_arr = substr_count_dictionary.get(substr)
        if count_arr == None:
            substr_count_dictionary[substr] = [i]
        else:
            count_arr.append(i)
            substr_count_dictionary[substr] =  count_arr

    return substr_count_dictionary

# test
res = find_substring_locations("TTAATTAGGGGCGC", 3)
print(res)

import os

def count_lines(in_file, out_file):
    count = read_file(in_file)
    write_file(out_file, count)


def read_file(in_file):
    file = open(in_file, "r")
    # content = f.read()
    count = 0
    for line in file:
        # print(count,")",line)
        count = count + 1
    file.close()
    return count


def write_file(out_file, content):
    result = open(out_file, "w")
    result.write(str(content)+ "\n")
    result.close()


# test code
count_lines("q4_input_example_1.txt", "number_of_lines.txt")
